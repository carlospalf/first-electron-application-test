/**
 * Draws the canvas surface and fill with random walls and the end/initial point
 */
function draw(){
    if($(".maze-canvas .element").length != 0){
      reset();
    }
    // Disable dimension input
    $(".dimension-input").get(0).disabled = true;
  
    // Get square dimension
    let dim = parseInt($(".dimension-input").get(0).value);
    // Calculate numbr of elements
    let elements = dim * dim;
  
    // make square the canvas
    let heightCanvas = parseInt($(".maze-canvas").height());
    $(".maze-canvas").height(heightCanvas);
    $(".maze-canvas").width(heightCanvas);
    let i = 0;
  
    // Loop for the creation of maze elements
    while(i < elements){
        $(".maze-canvas").append('<div class="element" onclick="selectInitialCell(event)"></div>');
      i++;
    }
  
    // Applying initial styles to maze elements
    size = ((heightCanvas / parseInt(dim)) -1);
    $(".maze-canvas .element").css({"height": size + "px", "width": size + "px"});
  }
  
  /**
   * Reset the canvas to initial state (empty)
   */
  function reset(){
    // Disable dimension input
    $(".dimension-input").get(0).disabled = false;
    // Delete elements from canvas
    $(".maze-canvas .element").remove();
    // Clear canvas styles
    $(".maze-canvas").removeAttr("style");
  }
  
  /**
   * Start the algorithm execution
   */
  function start(){
    let actualElement = $($(".maze-canvas .element.actual").get(0));
    let visitedElement = $($(".maze-canvas .element.visited").get(0));
    
    if(actualElement.length && !visitedElement.length){
      move(actualElement.index());
    }
  }
  
  /**
   * Check if this maze cell has up neighbour
   * @param {Number} index 
   */
  function checkUp(index){
    let dim = parseInt($(".dimension-input").get(0).value);
    let newIndex = index - dim;
  
    if( newIndex < 0) {
      return -1;
    } else {
      if(isValidCell(newIndex)){
        return newIndex; 
      }
      return -1;
    }
  }
  
  /**
   * Check if this maze cell has down neighbour
   * @param {Number} index 
   */
  function checkDown(index){
    let dim = parseInt($(".dimension-input").get(0).value);
    let newIndex = index + dim;
    let numElements = dim * dim;
  
    if(newIndex >= numElements) {
      return -1;
    } else {
      if(isValidCell(newIndex)){
        return newIndex; 
      }
  
      return -1;
    }
  }
  
  /**
   * Check if this maze cell has left neighbour
   * @param {Number} index 
   */
  function checkLeft(index){
    let dim = parseInt($(".dimension-input").get(0).value);
    let newIndex = index - 1;
  
    if(newIndex < 0 || newIndex % dim == (dim - 1)){
      return -1;
    } else {
      if(isValidCell(newIndex)){
        return newIndex; 
      }
  
      return -1;
    }
  }
  
  /**
   * Check if this maze cell has right neighbour
   * @param {Number} index 
   */
  function checkRight(index){
    let dim = parseInt($(".dimension-input").get(0).value);
    let newIndex = index + 1;
    let numElements = dim * dim;
  
    if(newIndex >= numElements || newIndex % dim == 0){
      return -1;
    } else {
      if(isValidCell(newIndex)){
        return newIndex; 
      }
  
      return -1;
    }
  }
  
  /**
   * Check if the index can be visited or not
   * @param {Number} index 
   */
  function isValidCell(index){
    return !$($(".maze-canvas .element").get(index)).hasClass("visited")
  }
  
  /**
   * Do the correspondent move on the maze and check if the maze has been alredy solved
   * @param {Number} index 
   */
  function move(index){
  let possibleDirectionArray = getPossibleDirectionsArray(index);
  let destiny = randomMovement(possibleDirectionArray);
  
    if(typeof destiny.index != "undefined") { // Si se mueve
        markAsVisited(index, destiny);
  
      if(!isFinished()){
        setTimeout(function () {
          move(destiny.index);
        }, 50);
      } else {
        $($(".maze-canvas .element").get(index)).removeClass("actual");
        $($(".maze-canvas .element").get(index)).addClass("visited end");
        alert("Successfull solution")
      }
    } else { // Si no se puede mover
      $($(".maze-canvas .element").get(index)).removeClass("actual");
      $($(".maze-canvas .element").get(index)).addClass("visited");
      if(!isFinished()){
        let newIndex = getVisitedWithUnvisitedNeighbours();
  
        setTimeout(function () {
          move(newIndex);
        }, 50);
        // Realizar comprobación de si esta encerrado
      } else {
        $($(".maze-canvas .element").get(index)).addClass("end");
        setTimeout(function(){
          alert("Maze successfully generated");
        }, 100);
      }
    }
  }
  
  /**
   * Change maze cells style to mar them as actual and visited.
   * @param {Number} source 
   * @param {Object} dest 
   */
  function markAsVisited(source, dest){
    let destDirection = dest.dir;
    let destIndex = dest.index;
  
    // Modify styles of source cell
    $($(".maze-canvas .element").get(source)).removeClass("actual");
    $($(".maze-canvas .element").get(source)).addClass("visited");
    
    // Modify styles of dest cell
    $($(".maze-canvas .element").get(destIndex)).addClass("actual");
  
    // Remove walls
    if(destDirection == "UP"){
      $($(".maze-canvas .element").get(source)).addClass("exit-up");
      $($(".maze-canvas .element").get(destIndex)).addClass("exit-down");
    }
  
    if(destDirection == "DOWN"){
      $($(".maze-canvas .element").get(source)).addClass("exit-down");
      $($(".maze-canvas .element").get(destIndex)).addClass("exit-up");
    }
  
    if(destDirection == "LEFT"){
      $($(".maze-canvas .element").get(source)).addClass("exit-left");
      $($(".maze-canvas .element").get(destIndex)).addClass("exit-right");
    }
  
    if(destDirection == "RIGHT"){
      $($(".maze-canvas .element").get(source)).addClass("exit-right");
      $($(".maze-canvas .element").get(destIndex)).addClass("exit-left");
    }
  }
  
  /**
   * Check if the maze has been solved
   */
  function isFinished(){
    return $(".maze-canvas .element").not($(".visited") || $(".actual")).length < 1
  }
  
  /**
   * Calculate the possible directions on maze cell
   * @param {Array} index 
   */
  function getPossibleDirectionsArray(index){
    let possibleDirectionArray = [];
  
    let upValue = checkUp(index);
    let downValue = checkDown(index);
    let leftValue = checkLeft(index);
    let rightValue = checkRight(index);
  
    if(upValue != -1){
      possibleDirectionArray.push({dir: "UP", index: upValue});
    }
  
    if(downValue != -1){
      possibleDirectionArray.push({dir: "DOWN", index: downValue});
    }
  
    if(leftValue != -1){
      possibleDirectionArray.push({dir: "LEFT", index: leftValue});
    }
  
    if(rightValue != -1){
      possibleDirectionArray.push({dir: "RIGHT", index: rightValue});
    }
  
    if(possibleDirectionArray.length > 1){
        $($(".maze-canvas .element").get(index)).addClass("possibly-with-neighbours");
    } else {
        $($(".maze-canvas .element").get(index)).removeClass("possibly-with-neighbours");
    }
    return possibleDirectionArray; 
  }
  
  /**
   * Choose randomly one of the possible directions of the array
   * @param {Object} possibleDirectionArray 
   */
  function randomMovement(possibleDirectionArray){
    let arrayLength = possibleDirectionArray.length;
    if(arrayLength != 0){
      let randomIndex = Math.floor(Math.random(Date.now()) * arrayLength)
      let randomArrayMovement = possibleDirectionArray[randomIndex]
  
      return randomArrayMovement;
    }
  
    return {};
  }
  
  /**
   * Selects the initial cell of maze generation algorithm
   * @param {Event} e 
   */
  function selectInitialCell(e){
    if(!$(".maze-canvas .element.initial").length){
      $(e.target).addClass("initial actual");
    }
  }
  
  /**
   * Gets the visited maze cell with unvisited neighbours
   */
  function getVisitedWithUnvisitedNeighbours(){
    let visitedElements = $(".maze-canvas .element.visited.possibly-with-neighbours")
    let i = 0;
  
    while(i < visitedElements.length){
      let possibleDirectionArray = getPossibleDirectionsArray($(visitedElements.get(i)).index());
      if(possibleDirectionArray.length > 0){
        return $(visitedElements.get(i)).index();
      }
      i++;
    }
    
    return -1;
  }